/// <reference types="cypress" />

describe("Requester submit PO", () => {
  it("Submit this PO", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/0/58080038/DIT",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/58080038");
    cy.wait("@getData");

    cy.get(
      ".fixTableHead table tbody tr#_1.accordion.border-b.border-grey-light.bg-blue-100 td span#_1.cursor-pointer"
    ).click();
    cy.wait(1500);
    cy.get(".footer button#next_button").click();
    cy.wait(2000);
    cy.get(".rating .A .B .C .divstar ul label#l5").click();
    cy.get(".rating .A .B2 .C .divstar2 ul label#l5_2").click();
    cy.get(".footer .A .B textarea").type(
      "จำนวนถูกต้องและสเปคได้ตามที่ต้องการ เอาไปเรย 5 ดาว♥"
    );
    cy.get(".subm .A .B button#subm").click();
  });
});
