/// <reference types="cypress" />

describe("Requester insert img", () => {
  it("img insert", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/0/58080038/DIT",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/58080038");
    cy.wait("@getData");

    cy.get(
      ".fixTableHead table tbody tr#_1.accordion.border-b.border-grey-light.bg-blue-100 td span#_1.cursor-pointer"
    ).click();
    cy.wait(1500);
    cy.get(".footer button#next_button").click();
    cy.wait(2000);

    const filepath = "img/test.JPG";
    cy.get(".divImg label#l_img .divClickimg svg").attachFile(filepath);
    //ติดอยู่ เนื่องจากให้เพิ่มรูปภาพหลังจากกดเพิ่มรูปทันที ไม่ได้มีปุ่มแยก เลยไม่รู้จะกด submit ตรงไหน
    // cy.get("#file-submit").click();
    // cy.get("#uploaded-files").contains("test.JPG");
  });
});
