/// <reference types="cypress" />

describe("Requester comment page1", () => {
  it("Comment QR Page", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/1/52110071/MMM",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/52110071");
    cy.wait("@getData");

    cy.get(
      ".fixTableHead table tbody tr#_1.accordion.border-b.border-grey-light.bg-blue-100 td span#_1.cursor-pointer"
    ).click();
    cy.wait(2000);
    cy.get(".commentarea textarea").type("ยังขาดอยู่จำนวน 2 ชิ้น รอติดตาม");
  });
});
