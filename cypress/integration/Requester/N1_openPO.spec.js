/// <reference types="cypress" />

describe("Requester click  PO", () => {
  it("Go to detail in PO page", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/1/52110071/MMM",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/52110071");
    cy.wait("@getData");

    cy.get(
      ".fixTableHead table tbody tr#_1.accordion.border-b.border-grey-light.bg-blue-100 td span#_1.cursor-pointer"
    ).click();
  });
});
