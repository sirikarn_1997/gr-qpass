/// <reference types="cypress" />

describe("Requester click  PO", () => {
  it("Go to detail in PO page", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/0/58080038/DIT",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/58080038");
    cy.wait("@getData");

    cy.get(
      ".fixTableHead table tbody tr#_1.accordion.border-b.border-grey-light.bg-blue-100 td span#_1.cursor-pointer"
    ).click();
  });
});
