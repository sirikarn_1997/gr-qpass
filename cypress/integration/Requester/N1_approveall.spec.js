/// <reference types="cypress" />

describe("Requester can click near PO to see detail in PO", () => {
  it("Show Detail in PO", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/1/52110071/MMM",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/52110071");
    cy.wait("@getData");

    cy.get(".fixTableHead table thead tr th span input").click();
    cy.get("#apa_enable").click();
  });
});
