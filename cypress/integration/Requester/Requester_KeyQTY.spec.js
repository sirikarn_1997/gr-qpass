/// <reference types="cypress" />

describe("Requester edit qty in PO", () => {
  it("Edit QTY in PO", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/0/58080038/DIT",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/58080038");
    cy.wait("@getData");

    cy.get(
      ".fixTableHead table tbody tr#_1.accordion.border-b.border-grey-light.bg-blue-100 td span#_1.cursor-pointer"
    ).click();
    cy.wait(3000);
    cy.get(".A .B table tr#1 td#ip_1 input#enterQTY").clear().type("4");
  });
});
