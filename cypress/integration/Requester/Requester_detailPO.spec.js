/// <reference types="cypress" />

describe("Requester can click near PO to see detail in PO", () => {
  it("Show Detail in PO", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/0/58080038/DIT",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/58080038");
    cy.wait("@getData");

    cy.get("#2_open").click();
  });
});
