/// <reference types="cypress" />

describe("Requester can key PO,GR,Vendor for search", () => {
  it("Search by input", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/0/58080038/DIT",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/58080038");
    cy.wait("@getData");

    cy.get("#PO").type("48100601*");
    // cy.get("#Vendor").type("ไทย*");
    cy.get('[name="ppur"]').select("(สแตมป์) สุพัตรา อินนัดดา");
    cy.get("#Find_QR").click();
  });
});
