/// <reference types="cypress" />

describe("Requester submit PO", () => {
  it("Submit this PO", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/purchase/39110004/DPC",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/39110004");
    cy.wait("@getData");

    cy.get(
      ".fixTableHead table tbody tr#_2.accordion.border-b.border-grey-light.bg-blue-100 td span#_2.cursor-pointer"
    ).click();
    cy.wait(1500);
    cy.get(".footer button#next_button").click();
    cy.wait(1500);
    cy.get(".rating .A .B .C .divstar ul label#l4").click();
    cy.get(".rating .A .B2 .C .divstar2 ul label#l5_2").click();
    cy.get(".footer .A .B textarea").type(
      "สเปคถูกต้องแต่จำนวนไม่ครบ คุณภาพและมาตรฐานอยู่ในเกณฑ์รับได้"
    );
    cy.get(".subm .A .B button#subm").click();
  });
});
