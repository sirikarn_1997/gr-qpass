/// <reference types="cypress" />

describe("Requester comment page1", () => {
  it("Comment QR Page", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/purchase/39110004/DPC",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/39110004");
    cy.wait("@getData");

    cy.get(
      ".fixTableHead table tbody tr#_2.accordion.border-b.border-grey-light.bg-blue-100 td span#_2.cursor-pointer"
    ).click();
    cy.wait(2000);
    cy.get(".commentarea textarea").type("ยังขาดสินค้าจำนวน 2 ชิ้น");
    cy.get(".commentarea textarea")
      .contains("ยังขาดสินค้าจำนวน 2 ชิ้น")
      .should("not.be.empty");
  });
});
