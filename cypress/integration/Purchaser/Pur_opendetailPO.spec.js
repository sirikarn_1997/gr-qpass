/// <reference types="cypress" />

describe("Purchase can click near PO to see detail in PO", () => {
  it("Show Detail in PO", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/purchase/39110004/DPC",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/39110004");
    cy.wait("@getData");

    cy.get("#1_open").click();
    cy.get(".minibox").should("be.visible");
  });
});
