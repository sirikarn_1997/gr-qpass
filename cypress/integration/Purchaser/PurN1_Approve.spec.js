/// <reference types="cypress" />

describe("Requester comment page1", () => {
  it("Comment QR Page", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/dep_purchase/64010005/DPC",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/64010005");
    cy.wait("@getData");

    cy.get(
      ".fixTableHead table tbody tr#_0.accordion.border-b.border-grey-light.bg-blue-100 td span#_0.cursor-pointer"
    ).click();
    cy.wait(2000);
    cy.get(".commentarea textarea").type("รับทราบ ได้รับครบถ้วน");
    cy.get(".footer2 #approved_click").click();
  });
});
