describe("แสดง Q-Pass List เฉพาะรายการที่ออก Q-Pass แล้วเท่านั้น", () => {
  before(() => {
    cy.visit("http://localhost:8080/auth/64010005");
  });

  it("LoggedIn", () => {
    cy.url().should("eq", "http://localhost:8080/detail_im/");
  });

  it("displays list Q-PASS (approved)", () => {
    cy.get("tbody").should("be.visible");
  });
});
