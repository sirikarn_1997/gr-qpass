/// <reference types="cypress" />

describe("Click Open File", () => {
  it("Open attached file", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/dep_purchase/64010005/DPC",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/64010005");
    cy.wait("@getData");

    cy.get(
      ".fixTableHead table tbody tr#_1.accordion.border-b.border-grey-light.bg-blue-100 td span#_1.cursor-pointer"
    ).click();
    cy.wait(2000);
    cy.get(".img span").click();
    cy.get(".modal-overlay").should("be.visible");
  });
});
