/// <reference types="cypress" />

describe("Requester can key PO,GR,Vendor for search", () => {
  it("Search by input", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/purchase/39110004/DPC",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/39110004");
    cy.wait("@getData");
    cy.wait(500);
    // cy.get("#Vendor").type("ไทย*");
    cy.get('[name="ppur"]').select("(ตุ๊ก) บงกชรัตน์ ด้วงขำ");
    cy.get("#Find_QR").click();

    cy.get('[name="ppur"]')
      .contains("(ตุ๊ก) บงกชรัตน์ ด้วงขำ")
      .should("have.value", "(ตุ๊ก) บงกชรัตน์ ด้วงขำ");
  });
});
