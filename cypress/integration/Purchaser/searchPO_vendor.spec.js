describe("ค้นหาเฉพาะ PO หรือ Vendor name หรือ เฉพาะชื่อตัวเองที่ออก PO โดยมี Dropdown List ให้", () => {
  before(() => {
    cy.visit("http://localhost:8080/auth/64010005");
  });

  it("PO Search Found", () => {
    cy.get("#PO").type("4810057929");
    cy.get("#Find_QR").click();
    cy.get("table").contains("td", "4810057929").should("be.visible");
  });

  it("PO Search NOT Found", () => {
    cy.get("#PO").type("1234");
    cy.get("#Find_QR").click();

    cy.on("window:alert", (text) => {
      expect(text).to.contains("ไม่พบข้อมูล");
    });
  });

  it("Vendor Name Search Found", () => {
    cy.get("#Vendor").type("*เอ็น*");
    cy.get("#Find_QR").click();
    cy.get("table").contains("td", "เอ็น").should("be.visible");
  });

  it("Vendor Name Search Not Found", () => {
    cy.get("#Vendor").type("*หาไม่เจอ*");
    cy.get("#Find_QR").click();
    cy.on("window:alert", (text) => {
      expect(text).to.contains("ไม่พบข้อมูล");
    });
  });
});
