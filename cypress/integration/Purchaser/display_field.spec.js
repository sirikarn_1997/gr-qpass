describe("แสดงข้อมูล PO,GR,วันที่รับ,ชื่อผู้ขาย,Section,Pruchase Name", () => {
  before(() => {
    cy.visit("http://localhost:8080/auth/64010005");
  });

  it("PO", () => {
    cy.get(".flex > #_0").should("not.be.empty");
  });
  it("GR", () => {
    cy.get(":nth-child(2) > .accordion > :nth-child(3)").should("not.be.empty");
  });
  it("GR DATE", () => {
    cy.get(":nth-child(2) > .accordion > :nth-child(4)").should("not.be.empty");
  });
  it("Vendor", () => {
    cy.get(":nth-child(2) > .accordion > :nth-child(5)").should("not.be.empty");
  });
  it("Section", () => {
    cy.get(":nth-child(2) > .accordion > :nth-child(7)").should("not.be.empty");
  });
  it("Purchaser", () => {
    cy.get(":nth-child(2) > .accordion > :nth-child(8)").should("not.be.empty");
  });
});
