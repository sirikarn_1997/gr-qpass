/// <reference types="cypress" />

describe("Requester click  PO", () => {
  it("Go to detail in PO page", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/purchase/39110004/DPC",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/39110004");
    cy.wait("@getData");

    cy.get(
      ".fixTableHead table tbody tr#_2.accordion.border-b.border-grey-light.bg-blue-100 td span#_2.cursor-pointer"
    ).click();
  });
});
