/// <reference types="cypress" />

describe("Requester can click near PO to see detail in PO", () => {
  it("Show Detail in PO", async () => {
    cy.intercept({
      method: "GET",
      path: "/line-ci/grpass/qpass/getGR_Body/dep_purchase/64010005/DPC",
    }).as("getData");
    // when
    cy.visit("http://localhost:8080/auth/64010005");
    cy.wait("@getData");

    cy.get(".fixTableHead table thead tr th span input").click();
    cy.get("#apa_enable").click();

    cy.get(".fixTableHead table thead tr th span :checkbox").should(
      "be.disabled"
    );
  });
});
