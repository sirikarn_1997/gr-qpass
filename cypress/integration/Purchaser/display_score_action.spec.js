describe("แสดงข้อมูล PO,GR,วันที่รับ,ชื่อผู้ขาย,Section,Pruchase Name", () => {
  // before(() => {
  //   cy.visit("http://localhost:8080/auth/64010005");
  // });

  it("DEP", () => {
    cy.visit("http://localhost:8080/auth/64010005");
    cy.get("table").contains("th", "Score").should("exist");
    cy.get("table").contains("th", "Action").should("exist");
  });
  it("Purchaser", () => {
    cy.visit("http://localhost:8080/auth/39110004");
    cy.get("table").contains("th", "Score").should("not.exist");
    cy.get("table").contains("th", "Action").should("not.exist");
  });
});
