describe("สามารถคลี่ PO ดูข้อมูลใน PO ได้เพื่อให้เห็นจำนวนและมูลค่า", () => {
  before(() => {
    cy.visit("http://localhost:8080/auth/64010005");
  });

  it("Expand", () => {
    cy.get("#30 _open > .h-5").click();
    cy.get(
      ":nth-child(2) > #detail_list > :nth-child(4) > .flex > :nth-child(2)"
    ).should("be.visible");
  });
});
