import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Axios from "axios";
import "./index.css";
import "flowbite";
import VueSweetalert2 from "vue-sweetalert2";
import PerfectScrollbar from "vue2-perfect-scrollbar";
import "vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css";
import Embed from "v-video-embed";
import VueEasyLightbox from "vue-easy-lightbox";
import "tw-elements";
import vTitle from "vuejs-title";

Vue.use(Embed);
Vue.use(VueEasyLightbox);
Vue.config.productionTip = false;
Vue.use(vTitle);
import "sweetalert2/dist/sweetalert2.min.css";

Vue.use(PerfectScrollbar);
Vue.use(VueSweetalert2);
new Vue({
  router,
  store,
  Axios,
  render: (h) => h(App),
}).$mount("#app");
