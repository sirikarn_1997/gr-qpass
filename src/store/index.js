import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    search_t: {},
    isLogin: JSON.parse(localStorage.getItem("isLogin")),
    userData: JSON.parse(localStorage.getItem("userData")),
    fna: {
      date: [],
      vendor: null,
    },
  },
  mutations: {
    setSearchTerm(state, data) {
      state.search_t = data;
    },
    setUserData(state, data) {
      state.userData = data;
      state.isLogin = true;

      localStorage.setItem("isLogin", true);
      localStorage.setItem("userData", JSON.stringify(data));
    },
    setFilter(state, data) {
      state.fna.date = data.date;
      state.fna.vendor = data.vendor;
    },
    setLogOut(state) {
      state.userData = null;
      state.isLogin = null;

      localStorage.setItem("isLogin", null);
      localStorage.setItem("userData", null);
    },
  },
  getters: {
    search_t(state) {
      return state.search_t;
    },
    auth(state) {
      return state.isLogin;
    },
    fna(state) {
      return state.fna;
    },
  },
  actions: {},
  modules: {},
});
