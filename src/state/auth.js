import { reactive } from "vue";

const auth = reactive({
  role: null,
});

export { auth };
